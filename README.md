# OpenML dataset: Coronavirus-News-(COVID-19)

https://www.openml.org/d/43634

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The World Health Organization (WHO) declared the 201920 coronavirus outbreak a pandemic and a Public Health Emergency of International Concern (PHEIC). Evidence of local transmission of the disease has been found in many countries across all six WHO regions.
Content
Coronaviruses are a large family of viruses that are known to cause illness ranging from the common cold to more severe diseases such as Middle East Respiratory Syndrome (MERS) and Severe Acute Respiratory Syndrome (SARS).
A novel coronavirus (COVID-19) was identified in 2019 in Wuhan, China. This is a new coronavirus that has not been previously identified in humans, and has since spread globally, resulting in the 201920 coronavirus pandemic.
Acknowledgements
I'd like to express my gratitude to my instructional team (Essra Madi, Markus Lim, Fahad Alsharekh and Bilal Yousef) for all of there efforts.
Inspiration
I hope this data will help the data scientist to get an insight about spreading the (COVID-19) around the world.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43634) of an [OpenML dataset](https://www.openml.org/d/43634). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43634/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43634/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43634/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

